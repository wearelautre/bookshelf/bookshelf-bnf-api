package bzh.lautre.bookshelf.bnf.ws

import bzh.lautre.bookshelf.bnf.ws.Helpers.nodeToDocument
import bzh.lautre.bookshelf.bnf.ws.model.BookSearchResult
import bzh.lautre.bookshelf.bnf.ws.model.XmlMappable
import io.github.oshai.kotlinlogging.KotlinLogging
import org.apache.http.client.fluent.Content
import org.apache.http.client.fluent.Request
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.util.DefaultUriBuilderFactory
import org.w3c.dom.NodeList
import java.io.File
import java.io.FileOutputStream
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory
import kotlin.reflect.KClass


@Component
class BnfSearchApi {

    private val logger = KotlinLogging.logger {}

    @Value("\${bookshelf.covers.search.path}")
    private lateinit var coversSearchPath: String

    @Value("\${bookshelf.bnf.domain}")
    private lateinit var bnfDomain: String

    @Value("\${bookshelf.bnf.port}")
    private lateinit var bnfPort: String

    @Value("\${bookshelf.bnf.protocol}")
    private lateinit var bnfProtocol: String

    @Value("\${bookshelf.bnf.timeout.connect}")
    private lateinit var bnfTimeoutConnect: Number

    @Value("\${bookshelf.bnf.timeout.socket}")
    private lateinit var bnfTimeoutSocket: Number

    fun getBooksDataByIsbn(isbn: String, getCover: Boolean = true): List<BookSearchResult> {
        val operation = "searchRetrieve"
        val version = 1.2
        val maxRecord = 20
        val recordSchema = "unimarcxchange"
        val startRecord = 1
        val query = """bib.isbn all"$isbn"""".trimIndent()

        val uri = DefaultUriBuilderFactory()
            .builder()
            .scheme(bnfProtocol)
            .host(bnfDomain)
            .port(bnfPort.toInt())
            .path("/api/SRU")
            .queryParam("version", version)
            .queryParam("operation", operation)
            .queryParam("query", query)
            .queryParam("recordSchema", recordSchema)
            .queryParam("maximumRecords", maxRecord)
            .queryParam("startRecord", startRecord)
            .build()

        val bookList = ArrayList<BookSearchResult>()

        try {
            val dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
            logger.info { uri.toString() }
            val content: Content = Request.Get(uri)
                .connectTimeout(bnfTimeoutConnect.toInt())
                .socketTimeout(bnfTimeoutSocket.toInt())
                .execute()
                .returnContent()

            logger.info { (content.asString()) }

            val doc = dBuilder.parse(content.asStream())

            val records = XPathFactory
                .newInstance()
                .newXPath()
                .compile("/searchRetrieveResponse/records/record/recordData/record").evaluate(doc, XPathConstants.NODESET) as NodeList

            logger.debug { "Books found : ${records.length}" }

            for (i in 0 until records.length) {
                val bookSearchResult = BookMapper.toBook(nodeToDocument(records.item(i)), isbn)
                if (getCover) {
                    this.getBookCover(bookSearchResult)
                }
                bookList.add(bookSearchResult)
            }

        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }

        return bookList
    }

    private fun getBookCover(bookSearchResult: BookSearchResult) {

        val uri = DefaultUriBuilderFactory()
            .builder()
            .scheme(bnfProtocol)
            .host(bnfDomain)
            .port(bnfPort.toInt())
            .path("couverture")
            .queryParam("appName", "NE")
            .queryParam("idArk", bookSearchResult.arkId)
            .queryParam("couverture", 1)
            .build()

        val directory = File(coversSearchPath)
        if (!directory.exists()) {
            directory.mkdirs()
            if (directory.setWritable(true)) {
                throw Exception("Directory [$coversSearchPath] is not writable")
            }
            if (directory.setReadable(true)) {
                throw Exception("Directory [$coversSearchPath] is not readable")
            }
        }

        val coverPath = "${this.coversSearchPath}/${bookSearchResult.isbn!!.replace("-", "")}.jpg"
        logger.info { coverPath }
        try {
            val fos = FileOutputStream(coverPath)
            fos.write(
                Request.Get(uri)
                    .connectTimeout(bnfTimeoutConnect.toInt())
                    .socketTimeout(bnfTimeoutSocket.toInt())
                    .execute()
                    .returnContent()
                    .asBytes()
            )
            fos.close()

            if (!ImageHelper.isNoCoverImage(File(coverPath))) {
                bookSearchResult.cover = File(coverPath).name
                if (File(coverPath).setReadable(true, false)) {
                    throw Exception("The cover is not readable")
                }
                if (File(coverPath).setWritable(true, false)) {
                    throw Exception("The cover is not writable")
                }
                logger.info { "Cover available through BFN API" }
            } else {
                logger.error { "Cover not available through BFN API" }
                throw Exception("No cover available")
            }
        } catch (e: Exception) {
            logger.error { e.message }
            if (File(coverPath).delete()) {
                logger.error { "Cover not deleted" }
            }
            bookSearchResult.cover = null
        }
    }

    class Attribute (
        val name: String,
        val value: String? = null
    )

    open class TagDescription(
        val tagName: String,
        val attribute: Attribute,
        val target: String? = null,
        val secondary: Boolean = false,
        val required: Boolean = false,
        val unique: Boolean = true,
        val getter: ((value: Any, validationValue: Any?) -> Any?)? = null,
        val children: List<TagDescription> = emptyList(),
        val targetClass: KClass<out XmlMappable>? = null,
        val subObject: Boolean = false,
        val subObjectTargetClass: KClass<out XmlMappable>? = null,
    )

    class DatafieldDescription<T: XmlMappable>(
        attribute: Attribute,
        children: List<TagDescription> = emptyList(),
        target: String? = null,
        unique: Boolean = true,
        subObject: Boolean = false,
        targetClass: KClass<T>? = null,
        subObjectTargetClass: KClass<T>? = null,
    ): TagDescription("datafield", attribute, target, unique = unique, children = children, targetClass = targetClass, subObject = subObject, subObjectTargetClass = subObjectTargetClass)

    class RecordDescription<in T: XmlMappable>(
        attribute: Attribute,
        target: String,
        children: List<TagDescription> = emptyList(),
        targetClass: KClass<T>? = null,
    ): TagDescription("record", attribute, target, children = children, targetClass = targetClass)

    class SubfieldDescription(
        attribute: Attribute,
        target: String,
        required: Boolean = false,
        unique: Boolean = true,
        secondary: Boolean = false,
        getter: ((value: Any, validationValue: Any?) -> Any?)? = null,
        targetClass: KClass<XmlMappable>? = null,
        ): TagDescription(
        tagName = "subfield",
        attribute = attribute,
        target = target,
        secondary = secondary,
        required = required,
        unique = unique,
        getter = getter,
        targetClass = targetClass
    )
}
