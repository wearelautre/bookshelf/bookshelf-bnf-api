package bzh.lautre.bookshelf.bnf.ws.model

class BookSearchResult(
        var arkId: String? = null,
        var price: String? = null,
        var series: String? = null,
        var editor: String? = null,
        var tome: Int? = null,
        var title: String? = null,
        var subTitle: String? = null,
        var artists: MutableList<ArtistSearchResult> = mutableListOf(),
        var collection: String? = null,
        var isbn: String? = null,
        var year: String? = null,
        var cover: String? = null,
        var rolesByArtists: MutableList<String> = mutableListOf(),
        var rawBnFData: String? = null
): XmlMappable
