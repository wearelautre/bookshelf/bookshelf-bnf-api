package bzh.lautre.bookshelf.bnf.ws

import bzh.lautre.bookshelf.bnf.ws.BnfSearchApi.SubfieldDescription
import bzh.lautre.bookshelf.bnf.ws.model.ArtistSearchResult
import bzh.lautre.bookshelf.bnf.ws.model.BookSearchResult
import bzh.lautre.bookshelf.bnf.ws.model.XmlMappable
import io.github.oshai.kotlinlogging.KotlinLogging
import org.w3c.dom.Document
import org.w3c.dom.NodeList
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty1
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.memberProperties

class BookMapper {
    companion object {
        private val logger = KotlinLogging.logger {}
        private val xPath: XPath = XPathFactory.newInstance().newXPath()

        private val mapCodeRole = mapOf(
            "070" to "scénario",
            "440" to "dessin",
            "730" to "traduction",
            "410" to "couleurs",
        )

        private val artistFields = listOf(
            SubfieldDescription(
                attribute = BnfSearchApi.Attribute(name = "code", value = "3"),
                target = "bnfId"
            ),
            SubfieldDescription(
                attribute = BnfSearchApi.Attribute(name = "code", value = "a"),
                target = "lastname"
            ),
            SubfieldDescription(
                attribute = BnfSearchApi.Attribute(name = "code", value = "b"),
                target = "firstname"
            ),
            SubfieldDescription(
                attribute = BnfSearchApi.Attribute(name = "code", value = "4"),
                unique = false,
                target = "roles"
            ),
        )

        private val record = BnfSearchApi.RecordDescription(
            attribute = BnfSearchApi.Attribute(name = "id"),
            target = "arkId",
            targetClass = BookSearchResult::class,
            children = listOf(
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "010"),
                    children = listOf(
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "a"),
                            target = "isbn",
                            required = true,
                            getter = { value, isbn ->
                                (value as List<*>).firstOrNull { sanitizeIsbn(it as String) == sanitizeIsbn(isbn as String) }
                            },
                            unique = false
                        ),
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "d"),
                            target = "price",
                        )
                    )
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "200"),
                    children = listOf(
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "a"),
                            target = "title"
                        ),
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "e"),
                            target = "subTitle"
                        ),
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "i"),
                            target = "subTitle",
                            secondary = true
                        ),
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "h"),
                            target = "tome",
                            getter = { value, _ ->
                                sanitizeTome(value as String)
                            }
                        ),
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "f"),
                            target = "rolesByArtists",
                            unique = false
                        ),
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "g"),
                            target = "rolesByArtists",
                            unique = false
                        )
                    )
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "210"),
                    children = listOf(
                        SubfieldDescription(attribute = BnfSearchApi.Attribute(name = "code", value = "c"), "editor"),
                        SubfieldDescription(attribute = BnfSearchApi.Attribute(name = "code", value = "d"), "year"),
                    )
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "225"),
                    children = listOf(
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "v"),
                            "tome",
                            secondary = true,
                            getter = { value, _ ->
                                sanitizeTome(value as String)
                            }),
                        SubfieldDescription(attribute = BnfSearchApi.Attribute(name = "code", value = "a"), "series"),
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "t"),
                            "series",
                            secondary = true
                        )
                    )
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "214"),
                    children = listOf(
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "c"),
                            "editor",
                            secondary = true
                        ),
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "d"),
                            "year",
                            secondary = true
                        ),
                    )
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "410"),
                    children = listOf(
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "t"),
                            "collection"
                        ),
                    )
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "461"),
                    children = listOf(
                        SubfieldDescription(
                            attribute = BnfSearchApi.Attribute(name = "code", value = "t"),
                            "series"
                        ),
                    )
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "700"),
                    unique = false,
                    target = "artists",
                    subObject = true,
                    subObjectTargetClass = ArtistSearchResult::class,
                    children = artistFields
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "701"),
                    unique = false,
                    target = "artists",
                    subObject = true,
                    subObjectTargetClass = ArtistSearchResult::class,
                    children = artistFields
                ),
                BnfSearchApi.DatafieldDescription(
                    attribute = BnfSearchApi.Attribute(name = "tag", value = "702"),
                    unique = false,
                    target = "artists",
                    subObject = true,
                    subObjectTargetClass = ArtistSearchResult::class,
                    children = artistFields
                )
            )
        )

        private fun isSeriesIsCollection(collection: String?, series: String?): Boolean {
            if (collection != null && series != null) {
                return collection.contains(series, true)
            }
            return false
        }

        private fun sanitizeIsbn(isbn: String): String {
            return isbn.replace("-", "").replace(" ", "").trim { it <= ' ' }
        }

        fun toBook(doc: Document, isbn: String): BookSearchResult {

            val bookSearchResult = setFields<BookSearchResult>(
                doc,
                record,
                validationValues = mapOf(
                    "isbn" to isbn
                )
            ) as BookSearchResult

            bookSearchResult.artists.forEach { artist ->
                if (artist.roles.isEmpty()) {
                    artist.roles = mutableListOf("unspecified")
                }
            }

            if (bookSearchResult.subTitle != null) {
                bookSearchResult.title += " " + bookSearchResult.subTitle
            }

            // Update series
            if (
                bookSearchResult.tome === null &&
                (bookSearchResult.series == "" || bookSearchResult.series == null || isSeriesIsCollection(
                    bookSearchResult.collection,
                    bookSearchResult.series
                ))
            ) {
                bookSearchResult.series = bookSearchResult.title
            }

            bookSearchResult.artists.forEach { artist ->
                artist.roles = artist.roles.map { mapCodeRole[it] ?: it }.toSet().toMutableList()
            }

            // Update artists
            // From rolesByArtists to artists
            parseArtistsFromRawData(bookSearchResult.rolesByArtists).forEach { artist ->
                val artistFound = bookSearchResult.artists.find { it.name == artist.name }
                if (artistFound != null) {
                    // only add new roles
                    artist.roles.removeAll(artistFound.roles)
                    artistFound.roles.addAll(artist.roles)
                } else {
                    bookSearchResult.artists.add(artist)
                }
            }

            bookSearchResult.rawBnFData = Helpers.nodeToString(doc).trimIndent().trim()
            return bookSearchResult

        }

        private fun parseArtistsFromRawData(rolesByArtists: List<String>): MutableList<ArtistSearchResult> {
            val artists = mutableListOf<ArtistSearchResult>()
            rolesByArtists.map { cleanArtistString(it) }.map {
                val matcher = Pattern.compile("^(.*(?= )|(?:[^,\\.]*))(?:, | par | de )(.*)").matcher(it)
                if (matcher.find()) {
                    artists.addAll(getArtistWithRole(matcher, matcher.group(1), matcher.group(2)))
                } else if (it != "") {
                    artists.add(
                        ArtistSearchResult(roles = mutableListOf("unspecified"), firstname = it)
                    )
                } else {
                    logger.warn { "The string $it is not parsable" }
                }
            }

            return artists
        }


        private fun getArtistWithRole(
            matcher: Matcher,
            roleString: String?,
            artistsString: String?
        ): List<ArtistSearchResult> {
            val list = mutableListOf<ArtistSearchResult>()
            if (matcher.group(0) != null) {
                val roles = getRoles(roleString ?: "unspecified")
                // Split artists by ","
                val artistMatcher = Pattern.compile("([^,]+)").matcher(artistsString)
                while (artistMatcher.find()) {
                    list.add(ArtistSearchResult(roles = roles, firstname = artistMatcher.group(1)))
                }
            }

            return list
        }

        private fun getRoles(role: String): MutableList<String> {
            val roles = mutableListOf<String>()
            // Split roles by " & " or " et "
            val matcher = Pattern.compile("^(.*)(?: (?:&|et) )(.*)\$").matcher(role)
            if (matcher.find()) {
                roles.add(matcher.group(1))
                roles.add(matcher.group(2))
            } else {
                roles.add(role)
            }
            return roles
        }

        private fun sanitizeTome(tome: String): Int? {
            val m = Pattern.compile("(\\d+)").matcher(tome)
            return if (m.find()) Integer.parseInt(m.group(1)) else null
        }

        private fun getDataExpressionForMapping(mapping: BnfSearchApi.TagDescription, rootExpression: String): String {
            val dataExpression = if (mapping.attribute.value != null) {
                rootExpression + "[@${mapping.attribute.name}='${mapping.attribute.value}']"
            } else {
                rootExpression + "/@${mapping.attribute.name}"
            }
            logger.debug { "dataExpression : $dataExpression" }
            return dataExpression
        }

        private fun cleanArtistString(s: String): String {
            logger.debug { "Clean artist string : $s" }
            var cleanString = s.trim()
            if (cleanString.startsWith("[") && cleanString.endsWith("]")) {
                cleanString = cleanString.substring(1, cleanString.length - 1).trim()
                cleanString = cleanString.replace("\\\"", "").trim()
            }
            cleanString = cleanString.removeSuffix("...").trim()
            cleanString = cleanString.removeSuffix("[et al.]").trim()
            cleanString = cleanString.removeSuffix("...").trim()
            logger.debug { "Cleaned artist string : $cleanString" }
            return cleanString
        }

        private fun getRootExpressionForMapping(
            mapping: BnfSearchApi.TagDescription,
            baseExpression: String = ""
        ): String {
            var rootExpression = baseExpression + "/${mapping.tagName}"
            rootExpression = if (mapping.attribute.value != null) {
                "$rootExpression[@${mapping.attribute.name}='${mapping.attribute.value}']"
            } else {
                rootExpression
            }
            logger.debug { "rootExpression : $rootExpression" }
            return rootExpression
        }

        private fun <T: XmlMappable> setFields(
            doc: Document,
            mapping: BnfSearchApi.TagDescription,
            targetObject: T? = null,
            validationValues: Map<String, Any> = emptyMap(),
            baseExpression: String = "",
            targetClass: KClass<out T>? = null
        ): XmlMappable {
            if (mapping.required && mapping.target == null) {
                throw Exception("Required field doesn't have target")
            }

            val rootExpression: String = getRootExpressionForMapping(mapping, baseExpression)
            val dataExpression: String = getDataExpressionForMapping(mapping, rootExpression)

            val currentTargetClass: KClass<out XmlMappable> = targetClass ?: mapping.targetClass ?: throw Exception("No target class found")
            logger.debug { "currentTargetClass : $currentTargetClass" }

            val currentTargetObject = (targetObject ?: currentTargetClass.createInstance())

            if (mapping.target != null) {
                val property = getClassProperty(currentTargetClass, mapping)
                if (mapping.secondary && property.getter.call(currentTargetObject) != null) {
                    logger.debug { "${mapping.tagName} with ${mapping.attribute.name}=${mapping.attribute.value} is secondary and data is already set for ${mapping.target}" }
                    return currentTargetObject
                }

                logger.debug { "Set ${mapping.target}" }
                val subfield = xPath
                    .compile(dataExpression)
                    .evaluate(doc, XPathConstants.NODESET) as NodeList

                if (subfield.length > 0) {
                    mapSubfield(property, mapping, subfield, currentTargetObject, validationValues)
                }
            } else {
                logger.debug { "${mapping.tagName} is a container no direct data to parse" }
            }

            return if (!mapping.subObject) {
                mapNonSubObjectField(mapping, currentTargetObject, doc, rootExpression, validationValues, currentTargetClass)
            } else {
                currentTargetObject
            }
        }

        private fun mapNonSubObjectField(
            mapping: BnfSearchApi.TagDescription,
            currentTargetObject: XmlMappable,
            doc: Document,
            rootExpression: String,
            validationValues: Map<String, Any>,
            currentTargetClass: KClass<out XmlMappable>
        ): XmlMappable {
            var currentTargetObject1 = currentTargetObject
            logger.debug { "${mapping.tagName} as children : ${mapping.children}" }
            for (child in mapping.children) {
                currentTargetObject1 =
                    setFields(
                        doc,
                        child,
                        currentTargetObject1,
                        baseExpression = rootExpression,
                        validationValues = validationValues,
                        targetClass = child.targetClass ?: currentTargetClass
                    )
            }
            return currentTargetObject1
        }

        private fun <T> mapSubfield(
            property: KProperty1<out Any, *>,
            mapping: BnfSearchApi.TagDescription,
            subfield: NodeList,
            currentTargetObject: T,
            validationValues: Map<String, Any>
        ) {
            if (property is KMutableProperty<*>) {
                var value = getValue(mapping, subfield, property, currentTargetObject, validationValues)
                value = updateValueWithGetter(mapping, value, validationValues)

                if (mapping.required && value == null) {
                    throw Exception("Required field ${mapping.target} is missing")
                }

                property.setter.call(currentTargetObject, value)
            } else {
                logger.debug { "Property ${mapping.target} is not mutable" }
            }
        }

        private fun getClassProperty(
            currentTargetClass: KClass<out Any>,
            mapping: BnfSearchApi.TagDescription
        ) = (currentTargetClass.memberProperties.find { it.name == mapping.target }
            ?: throw Exception("Property ${mapping.target} not found"))

        private fun updateValueWithGetter(
            mapping: BnfSearchApi.TagDescription,
            value: Any?,
            validationValues: Map<String, Any>
        ) = if (mapping.getter != null) {
            logger.debug { "Use specific getter for property ${mapping.target}" }
            mapping.getter.invoke(value!!, validationValues[mapping.target])
        } else {
            value
        }

        private fun <T> getValue(
            mapping: BnfSearchApi.TagDescription,
            subfield: NodeList,
            prop: KProperty1<out Any, *>,
            currentTargetObject: T,
            validationValues: Map<String, Any>
        ) = if (mapping.unique) {
            subfield.item(0).textContent
        } else {
            getNonUniqueFields(mapping, subfield, prop, currentTargetObject, validationValues)
        }

        private fun <T> getNonUniqueFields(
            mapping: BnfSearchApi.TagDescription,
            subfield: NodeList,
            prop: KProperty1<out Any, *>,
            currentTargetObject: T,
            validationValues: Map<String, Any>
        ): Any {
            val list = prop.getter.call(currentTargetObject) ?: mutableListOf<T>()
            val newElements = if (mapping.subObject) {
                val elements = mutableListOf<Any>()

                val clazz = mapping.subObjectTargetClass ?: throw Exception("No target class found for subObject ${mapping.target}")
                for (i in 0 until subfield.length) {
                    var subObjectTarget: XmlMappable  = clazz.createInstance()
                    for (child in mapping.children) {
                        subObjectTarget =
                            setFields(
                                Helpers.nodeToDocument(subfield.item(i)),
                                child,
                                subObjectTarget,
                                baseExpression = getRootExpressionForMapping(mapping),
                                validationValues = validationValues,
                                targetClass = mapping.subObjectTargetClass
                            )
                    }
                    elements.add(subObjectTarget)
                }
                elements
            } else {
                val elements = mutableListOf<String>()
                for (i in 0 until subfield.length) {
                    elements.add(subfield.item(i).textContent)
                }
                elements
            }

            try {
                list.javaClass.getMethod("addAll", Collection::class.java).invoke(list, newElements)
            } catch (e: Exception) {
                logger.error { e.message }
                throw Exception("Cannot add elements to list, is either not a list or not mutable")
            }

            return list
        }
    }
}