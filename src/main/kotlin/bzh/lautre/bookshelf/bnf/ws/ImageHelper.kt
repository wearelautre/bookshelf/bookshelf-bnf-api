package bzh.lautre.bookshelf.bnf.ws

import com.github.romankh3.image.comparison.ImageComparison
import com.github.romankh3.image.comparison.ImageComparisonUtil
import com.github.romankh3.image.comparison.model.ImageComparisonState
import org.slf4j.LoggerFactory
import org.springframework.context.support.ClassPathXmlApplicationContext
import java.io.File

class ImageHelper {
    companion object {
        private val logger = LoggerFactory.getLogger(ImageHelper::class.java)
        private val appContext = ClassPathXmlApplicationContext()

        @JvmStatic
        fun isNoCoverImage(newImage: File): Boolean {
            val file = appContext.getResource("classpath:images/no_cover.jpg")
            if (!file.isFile) {
                logger.error("The reference image doesn't exist")
                return false
            }

            return ImageComparison(
                    ImageComparisonUtil.readImageFromResources(newImage.absolutePath),
                    ImageComparisonUtil.readImageFromResources(file.file.absolutePath))
                    .compareImages()
                    .imageComparisonState === ImageComparisonState.MATCH
        }
    }

}
