package bzh.lautre.bookshelf.bnf.ws.model

class ArtistSearchResult(
    var bnfId: String? = null,
    var lastname: String? = null,
    var firstname: String? = null,
    var roles: MutableList<String> = mutableListOf()
): XmlMappable {

    val name: String
        get() = "${if (this.firstname != null) this.firstname else ""} ${if (this.lastname != null) this.lastname else ""}".trim()

}
