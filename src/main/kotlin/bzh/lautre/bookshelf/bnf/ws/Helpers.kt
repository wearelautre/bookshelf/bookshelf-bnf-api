package bzh.lautre.bookshelf.bnf.ws

import org.w3c.dom.Document
import org.w3c.dom.Node
import java.io.StringWriter

import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

internal object Helpers {

    fun nodeToDocument(node: Node): Document {
        return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(nodeToString(node).byteInputStream())
    }

    fun nodeToString(doc: Node): String {
        val buf = StringWriter()
        val xform = TransformerFactory.newInstance().newTransformer()
        xform.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes")
        xform.transform(DOMSource(doc), StreamResult(buf))
        return buf.toString()
    }

}
