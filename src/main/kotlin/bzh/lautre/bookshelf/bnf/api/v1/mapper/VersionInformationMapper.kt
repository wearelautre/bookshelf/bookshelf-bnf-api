package bzh.lautre.bookshelf.bnf.api.v1.mapper

import bzh.lautre.bookshelf.bnf.api.v1.model.VersionInformationDTO
import bzh.lautre.bookshelf.bnf.business.VersionService
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface VersionInformationMapper {

    fun map(versionInformation: VersionService.VersionInformation): VersionInformationDTO
}
