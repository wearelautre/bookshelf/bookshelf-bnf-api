package bzh.lautre.bookshelf.bnf.api.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springdoc.core.customizers.OpenApiCustomizer
import org.springdoc.core.customizers.OperationCustomizer
import org.springdoc.core.models.GroupedOpenApi
import org.springdoc.core.utils.Constants
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
class OpenApiGroupsConfiguration {

    @Bean
    @Profile("!prod")
    fun actuatorApi(
        actuatorOpenApiCustomizer: OpenApiCustomizer?,
        actuatorCustomizer: OperationCustomizer,
        endpointProperties: WebEndpointProperties,
        @Value("\${springdoc.version}") appVersion: String
    ): GroupedOpenApi {
        return GroupedOpenApi.builder()
            .group("Actuator")
            .pathsToMatch(endpointProperties.basePath + Constants.ALL_PATTERN)
            .addOpenApiCustomizer(actuatorOpenApiCustomizer)
            .addOpenApiCustomizer { openApi: OpenAPI ->
                openApi.info(
                    Info().title("Actuator API").version(appVersion)
                )
            }
            .addOperationCustomizer(actuatorCustomizer)
            .pathsToExclude("/health/*")
            .build()
    }

    @Bean
    open fun usersGroup(@Value("\${springdoc.version}") appVersion: String): GroupedOpenApi {
        return GroupedOpenApi.builder().group("BNF Search")
            .addOpenApiCustomizer { openApi: OpenAPI ->
                openApi.info(
                    Info().title("BNF Search API").version(appVersion)
                )
            }
            .packagesToScan("bzh.lautre.bookshelf.bnf")
            .build()
    }

}