package bzh.lautre.bookshelf.bnf.api.config

import io.swagger.v3.oas.annotations.OpenAPIDefinition

@OpenAPIDefinition(info = io.swagger.v3.oas.annotations.info.Info(title = "My App", description = "Some long and useful description", version = "v1"))
open class OpenApiConfig
