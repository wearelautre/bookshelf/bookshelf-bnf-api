package bzh.lautre.bookshelf.bnf.api.v1.mapper

import bzh.lautre.bookshelf.bnf.api.v1.model.BookDTO
import bzh.lautre.bookshelf.bnf.ws.model.BookSearchResult
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Named

@Mapper(componentModel = "spring")
abstract class BookMapper {

    @Mapping(target = "isbn", source = "isbn", qualifiedByName = ["sanitizeIsbn"])
    abstract fun map(bookSearchResult: BookSearchResult): BookDTO

    @Named("sanitizeIsbn")
    fun sanitizeIsbn(isbn: String): String {
        return isbn.replace("-", "")
            .replace(" ", "")
            .trim { it <= ' ' }
    }
}
