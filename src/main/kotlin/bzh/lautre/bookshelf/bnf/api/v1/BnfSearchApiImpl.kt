package bzh.lautre.bookshelf.bnf.api.v1

import bzh.lautre.bookshelf.bnf.api.v1.mapper.BookMapper
import bzh.lautre.bookshelf.bnf.api.v1.model.BookDTO
import bzh.lautre.bookshelf.bnf.ws.BnfSearchApi
import io.swagger.annotations.Api
import jdk.incubator.foreign.SymbolLookup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.RestController

@Validated
@RestController
@Api(tags = ["search"])
class BnfSearchApiImpl @Autowired
constructor(
    private val bnfSearchApi: BnfSearchApi,
    private val bookMapper: BookMapper
) : BnfApi {

    override fun searchBooks(isbn: String, withCover: Boolean?): ResponseEntity<List<BookDTO>> {
        return ResponseEntity.ok(bnfSearchApi.getBooksDataByIsbn(sanitizeIsbn(isbn), withCover ?: true).map(bookMapper::map))
    }

    private fun sanitizeIsbn(isbn: String) = isbn
        .replace("-", "")
        .replace(" ", "")
        .trim { it <= ' ' }

}

