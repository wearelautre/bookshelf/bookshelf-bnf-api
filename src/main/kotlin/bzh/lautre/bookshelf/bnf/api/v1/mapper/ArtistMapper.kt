package bzh.lautre.bookshelf.bnf.api.v1.mapper

import bzh.lautre.bookshelf.bnf.api.v1.model.ArtistDTO
import bzh.lautre.bookshelf.bnf.ws.model.ArtistSearchResult
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
fun interface ArtistMapper {

    fun map(artistSearchResult: ArtistSearchResult): ArtistDTO
}
