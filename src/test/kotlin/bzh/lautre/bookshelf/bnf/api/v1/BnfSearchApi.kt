package bzh.lautre.bookshelf.bnf.api.v1

import bzh.lautre.bookshelf.bnf.api.v1.model.ArtistDTO
import bzh.lautre.bookshelf.bnf.api.v1.model.BookDTO
import org.junit.jupiter.api.*
import org.mockserver.client.MockServerClient
import org.mockserver.integration.ClientAndServer
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.util.MultiValueMapAdapter
import java.nio.file.Files
import kotlin.io.path.toPath

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
internal class BnfSearchApiImplTest @Autowired constructor(
    val mockMvc: MockMvc
) {
    val baseUrl = "/bnf/search/books"

    lateinit var mockServer: MockServerClient

    @BeforeEach
    fun prepare() {
        mockServer = ClientAndServer.startClientAndServer(1080)
    }

    @AfterEach
    fun tearDown() {
        mockServer.close()
    }

    @Nested
    @DisplayName("GET /profile")
    inner class SearchBookByIsbn {

        @Test
        fun `should return the data parsed data for the isbn 9782803672462`() {
            createBook(
                isbn = "9782803672462",
                title = "La jeunesse Le drakkar des glaces",
                subTitle = "Le drakkar des glaces",
                tome = "6",
                editor = "le Lombard",
                year = "DL 2018",
                price = "12,45 EUR",
                arkId = "ark:/12148/cb455584870",
                cover = null,
                collection = null,
                series = "Les mondes de Thorgal",
                artists = mutableListOf(
                    ArtistDTO(
                        name = "Roman Surzhenko",
                        roles = mutableListOf("dessin", "couleurs"),
                    ), ArtistDTO(
                        name = "Yann",
                        roles = mutableListOf("scénario"),
                    ), ArtistDTO(
                        name = "R. Surzhenko, dessin",
                        roles = mutableListOf("unspecified"),
                    ), ArtistDTO(
                        name = "scénario",
                        roles = mutableListOf("Yann"),
                    )
                )
            ).let {
                callAndValidate(it.isbn!!, it)
            }
        }

        @Test
        fun `should return the data parsed data for the isbn 9782070584628`() {
            createBook(
                isbn = "9782070584628",
                title = "Harry Potter à l'école des sorciers",
                price = "8,70 EUR",
                tome = "1",
                year = "DL 2018",
                collection = "Collection Folio junior",
                editor = "Gallimard jeunesse",
                artists = mutableListOf(
                    ArtistDTO(
                        name = "J. K. Rowling",
                        roles = mutableListOf("scénario", "unspecified"),
                    ),
                    ArtistDTO(
                        name = "Jean-François Ménard",
                        roles = mutableListOf("traduction", "traduit de l'anglais"),
                    ),
                ),
                arkId = "ark:/12148/cb45595763x",
                series = "Harry Potter"
            ).let {
                callAndValidate(it.isbn!!, it)
            }
        }

        @Test
        fun `should return the data parsed data for the isbn 9782205073980`() {
            createBook(
                isbn = "9782205073980",
                title = "Blacksad l'intégrale",
                price = "49 EUR",
                subTitle = "l'intégrale",
                year = "DL 2014",
                editor = "Dargaud",
                artists = mutableListOf(
                    ArtistDTO(
                        name = "Juan Díaz Canales",
                        roles = mutableListOf("scénario"),
                    ),
                    ArtistDTO(
                        name = "Juanjo Guarnido",
                        roles = mutableListOf("dessin", "couleur"),
                    ),
                ),
                arkId = "ark:/12148/cb442318849",
                series = "Blacksad l'intégrale"
            ).let {
                callAndValidate(it.isbn!!, it)
            }
        }

        @Test
        fun `should return the data parsed data for the isbn 9782302015906`() {
            createBook(
                isbn = "9782302015906",
                title = "Lanfeust de Troy L'ivoire du Magohamoth",
                price = "13.50 EUR",
                subTitle = "L'ivoire du Magohamoth",
                tome = "1",
                year = "2011",
                editor = "Soleil",
                artists = mutableListOf(
                    ArtistDTO(
                        name = "Christophe Arleston",
                        roles = mutableListOf("scénario"),
                    ),
                    ArtistDTO(
                        name = "Didier Tarquin",
                        roles = mutableListOf("dessin"),
                    ),
                    ArtistDTO(
                        name = "Luca Saponti",
                        roles = mutableListOf("couleurs"),
                    ),
                    ArtistDTO(
                        name = "scénario Christophe Arleston",
                        roles = mutableListOf("unspecified"),
                    ),
                    ArtistDTO(
                        name = "dessins Didier Tarquin",
                        roles = mutableListOf("unspecified"),
                    ),
                    ArtistDTO(
                        name = "couleurs Luca Saponti",
                        roles = mutableListOf("unspecified"),
                    ),
                ),
                arkId = "ark:/12148/cb43479468x",
                series = "Lanfeust de Troy"
            ).let {
                callAndValidate(it.isbn!!, it)
            }
        }

        @Test
        fun `should return the data parsed data for the isbn 9782360140602`() {
            createBook(
                isbn = "9782360140602",
                title = "Shipwreck le naufrage",
                price = "17,50 EUR",
                subTitle = "le naufrage",
                year = "DL 2019",
                editor = "Snorgleux comics",
                artists = mutableListOf(
                    ArtistDTO(
                        name = "Warren Ellis",
                        roles = mutableListOf("scénario"),
                    ),
                    ArtistDTO(
                        name = "Phil Hester",
                        roles = mutableListOf("dessin"),
                    ),
                    ArtistDTO(
                        name = "créateur & scénariste",
                        roles = mutableListOf("Warren Ellis"),
                    ),
                    ArtistDTO(
                        name = "artiste",
                        roles = mutableListOf("Phil Hester"),
                    ),
                    ArtistDTO(
                        name = "encreur",
                        roles = mutableListOf("Eric Gapstur"),
                    ),
                ),
                arkId = "ark:/12148/cb456683397",
                series = "Shipwreck le naufrage"
            ).let {
                callAndValidate(it.isbn!!, it)
            }
        }

        fun createBook(
            isbn: String,
            title: String,
            subTitle: String? = null,
            tome: String? = null,
            editor: String,
            year: String,
            price: String? = null,
            arkId: String,
            cover: String? = null,
            collection: String? = null,
            series: String,
            artists: MutableList<ArtistDTO>
        ): BookDTO {
            val book = BookDTO()
            book.isbn = isbn
            book.title = title
            book.subTitle = subTitle
            book.tome = tome
            book.editor = editor
            book.year = year
            book.price = price
            book.arkId = arkId
            book.cover = cover
            book.collection = collection
            book.series = series
            book.artists = artists

            return book
        }

        fun callAndValidate(isbn: String, book: BookDTO) {
            mockServer.setup(
                "GET", "/api/SRU", isbn, 200, Files.readString(javaClass.getResource("/$isbn.xml")!!.toURI().toPath())
            )

            // Do not test cover call
            mockServer.`when`(
                request().withMethod("GET").withPath("/couverture").withQueryStringParameter("version", "1.2")
            ).respond(
                response().withStatusCode(404).withBody("Not Found")
            )
            mockMvc.get(baseUrl) {
                params = MultiValueMapAdapter(mapOf(
                    "isbn" to listOf(isbn),
                    "withCover" to listOf("false")
                ))
            }.andExpect {
                status { isOk() }
                println()
                content {
                    contentType(MediaType.APPLICATION_JSON_VALUE)
                    jsonPath("$.[0].title") { value(book.title) }
                    jsonPath("$.[0].subTitle") { value(book.subTitle) }
                    jsonPath("$.[0].tome") { value(book.tome) }
                    jsonPath("$.[0].editor") { value(book.editor) }
                    jsonPath("$.[0].year") { value(book.year) }
                    jsonPath("$.[0].price") { value(book.price) }
                    jsonPath("$.[0].arkId") { value(book.arkId) }
                    jsonPath("$.[0].isbn") { value(book.isbn) }
                    jsonPath("$.[0].cover") { value(book.cover) }
                    jsonPath("$.[0].collection") { value(book.collection) }
                    jsonPath("$.[0].series") { value(book.series) }
                    jsonPath("$.[0].artists[*].name") { value(book.artists!!.map { it.name }) }
                    jsonPath("$.[0].artists[*].roles") { value(book.artists!!.map { it.roles }) }
                }
            }

        }
    }

    internal fun MockServerClient.setup(
        requestMethod: String, requestPath: String, isbn: String, responseStatus: Int = 404, responseBody: String? = null
    ) {
        this.`when`(
            request().withMethod(requestMethod).withPath(requestPath).withQueryStringParameter("version", "1.2")
                .withQueryStringParameter("operation", "searchRetrieve")
                .withQueryStringParameter("query", """bib.isbn all"$isbn"""".trimIndent())
                .withQueryStringParameter("recordSchema", "unimarcxchange")
                .withQueryStringParameter("maximumRecords", "20").withQueryStringParameter("startRecord", "1")

        ).respond(
            response().withStatusCode(responseStatus).withBody(responseBody)
        )
    }

}
